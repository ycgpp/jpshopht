import request from '@/utils/request'

/**
 * 获取操作记录列表
 * @param {*} params 
 */
export function getLogList(params) {
  return request({
    url: '/Operation',
    method: 'get',
    params
  })
}