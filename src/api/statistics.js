import request from '@/utils/request'

/**
 * 销售统计
 * @param {*} params
 */
export function getSales(params) {
  return request({
    url: '/sales',
    method: 'get',
    params
  })
}

/**
 * 商品销售统计
 * @param {*} params
 */
export function getGoodsSales(params) {
  return request({
    url: '/goodsSales',
    method: 'get',
    params
  })
}

/**
 * 团长销售统计
 * @param {*} params
 */
export function getLeaderSales(params) {
  return request({
    url: '/leaderSales',
    method: 'get',
    params
  })
}

/**
 * 用户排行统计
 * @param {*} params
 */
export function getUserSales(params) {
  return request({
    url: '/userSales',
    method: 'get',
    params
  })
}
