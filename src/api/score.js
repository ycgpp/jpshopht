import request from '@/utils/request'


//积分商城

/**
 * 获取积分商品分组
 * @param {*} params 
 */
export function getScoreGoodsList(params) {
  return request({
    url: '/merchantScoreCategory',
    method: 'get',
    params
  })
}

/**
 * 新增积分商品分组
 * @param {*} params 
 */
export function postScoreGoodsGroup(params) {
  return request({
    url: '/merchantScoreCategory',
    method: 'post',
    data: params
  })
}
/**
 * 修改积分商品分组
 * @param {*} params 
 */
export function putScoreGoodsGroup(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantScoreCategory/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除积分商品分组
 * @param {*} params 
 */
export function delScoreGoodsGroup(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantScoreCategory/'+id,
    method: 'delete',
    data: params
  })
}


//积分商品列表

/**
 * 获取积分商品列表
 * @param {*} params 
 */
export function getGoodsList(params) {
  return request({
    url: '/merchantScoreGoods',
    method: 'get',
    params
  })
}
/**
 * 获取积分商品
 * @param {*} params 
 */
export function getGoods(params) {
  const id = params.id;
  delete params.id
  return request({
    url: '/merchantScoreGoods/'+id,
    method: 'get',
    params
  })
}
/**
 * 新增积分商品
 * @param {*} params 
 */
export function postScoreGoods(params) {
  return request({
    url: '/merchantScoreGoods',
    method: 'post',
    data: params
  })
}
/**
 * 修改积分商品
 * @param {*} params 
 */
export function putGoods(params) {
  const id = params.id;
  delete params.id
  return request({
    url: '/merchantScoreGoods/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除积分商品
 * @param {*} params 
 */
export function delScoreGoods(params) {
  const id = params.id;
  delete params.id
  return request({
    url: '/merchantScoreGoods/'+id,
    method: 'delete',
    data: params
  })
}


//积分订单

/**
 * 获取订单
 * @param {*} params 
 */
export function getOrder(params) {
  return request({
    url: '/merchantScoreOrder',
    method: 'get',
    params
  })
}
/**
 * 修改订单发货状态
 * @param {*} params 
 */
export function putOrderStatus(params) {
  return request({
    url: '/merchantScoreOrder',
    method: 'put',
    data: params
  })
}


//bannner

/**
 * 获取banner列表
 * @param {*} params 
 */
export function getBannerList(params) {
  return request({
    url: '/merchantScoreBanner',
    method: 'get',
    params
  })
}
/**
 * 新增banner
 * @param {*} params 
 */
export function postBanner(params) {
  return request({
    url: '/merchantScoreBanner',
    method: 'post',
    data: params
  })
}
/**
 * 修改banner
 * @param {*} params 
 */
export function putBanner(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantScoreBanner/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除banner
 * @param {*} params 
 */
export function delBanner(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantScoreBanner/'+id,
    method: 'delete',
    data: params
  })
}