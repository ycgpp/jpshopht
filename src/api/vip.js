import request from '@/utils/request'

//会员列表

/**
 * 获取会员列表
 * @param {key: xxgNZf,limit: 10,page: 1} params 
 */
export function getVipList(params) {
  return request({
    url: '/merchantShopUsers',
    method: 'get',
    params
  })
}
/**
 * 会员状态修改（拉黑）
 * @param {key,status,id} params 
 */
export function putVipStatus(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantShopUsers/'+id,
    method: 'put',
    data: params
  })
}

//会员卡

/**
 * 会员卡总开关查询
 * @param {key,englist_name} params 
 */
export function vipSwitch(params) {
  return request({
    url: '/merchantVipPlugin',
    method: 'get',
    params
  })
}
/**
 * 会员卡总开关修改
 * @param {key,english_name,is_open} params 
 */
export function putVipSwitch(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantVipPlugin/'+id,
    method: 'put',
    data:params
  })
}
/**
 * 获取付费会员列表
 * @param {key,searchName,limit,page} params 
 */
export function getPayVips(params) {
  return request({
    url: '/vips',
    method: 'get',
    params
  })
}
/**
 * 新增付费会员
 * @param {key,name,money,validity_time,pay_count,status} params 
 */
export function postPayVip(params) {
  return request({
    url: '/vips',
    method: 'post',
    data: params
  })
}
/**
 * 更新付费会员
 * @param {key,name,validity_time,pay_count,status,money} params 
 */
export function putPayVip(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/vips/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除付费会员
 * @param {*} params 
 */
export function delPayVip(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/vips/'+id,
    method: 'delete',
    data: params
  })
}

/**
 * 获取会员卡配置
 * @param {key} params 
 */
export function getVipConfig(params) {
  return request({
    url: '/vipConfig',
    method: 'get',
    params
  })
}
/**
 * 新增会员卡配置
 * @param {*} params 
 */
export function postVipConfig(params) {
  return request({
    url: '/vipConfig',
    method: 'post',
    data: params
  })
}
/**
 * 修改会员卡配置
 * @param {*} params 
 */
export function putVipConfig(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/vipConfig/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除会员卡配置
 * @param {*} params 
 */
export function delVipConfig(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/vipConfig/'+id,
    method: 'delete',
    data: params
  })
}
/**
 * 获取代金券类型
 * @param {*} params 
 */
export function getShopVouType(params) {
  return request({
    url: '/shopVouTypes',
    method: 'get',
    params
  })
}

//积分会员
/**
 * 获取积分会员列表
 * @param {key,searchName,limit,page} params 
 */
export function getUpPayVips(params) {
  return request({
    url: '/unpaidVips',
    method: 'get',
    params
  })
}
/**
 * 新增积分会员
 * @param {key,name,min_score,discount_ratio,voucher_count,voucher_type_id,score_times,status} params 
 */
export function postUnPayVip(params) {
  return request({
    url: '/unpaidVips',
    method: 'post',
    data: params
  })
}
/**
 * 修改积分会员
 * @param {*} params 
 */
export function putUnPayVip(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/unpaidVips/'+id,
    method: 'put',
    data: params
  })
}
/**
 * 删除积分会员
 * @param {*} params 
 */
export function delUpPayVip(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/unpaidVips/'+id,
    method: 'delete',
    data: params
  })
}