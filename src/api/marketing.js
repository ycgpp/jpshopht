import request from '@/utils/request'

//优惠券

/**
 * 获取优惠券列表
 * @param {*} params 
 */
export function getCouponList(params) {
  return request({
    url: '/shopVouTypes',
    method: 'get',
    params
  })
}
/**
 * 新增优惠券
 * @param {*} params 
 */
export function postCoupon(params) {
  return request({
    url: '/shopVouTypes',
    method: 'post',
    data: params
  })
}
/**
 * 修改优惠券
 * @param {*} params 
 */
export function putCoupon(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/shopVouTypes/' + id,
    method: 'put',
    data: params
  })
}
/**
 * 删除优惠券
 * @param {*} params 
 */
export function delCoupon(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/shopVouTypes/' + id,
    method: 'delete',
    data: params
  })
}
/**
 * 获取优惠券配置
 * @param {*} params 
 */
export function getcouponConfig(params) {
  return request({
    url: '/merchantShopConfig',
    method: 'get',
    params
  })
}
/**
 * 修改优惠券配置
 * @param {*} params 
 */
export function putcouponConfig(params) {
  return request({
    url: '/merchantShopConfig',
    method: 'put',
    data: params
  })
}


//秒杀
/**
 * 获取秒杀列表
 * @param {*} params 
 */
export function getSeckillList(params) {
  return request({
    url: '/merchantFlashSale',
    method: 'get',
    params
  })
}
/**
 * 添加秒杀
 * @param {*} params 
 */
export function postSeckill(params) {
  return request({
    url: '/merchantFlashSale',
    method: 'post',
    data: params
  })
}
/**
 * 修改秒杀
 * @param {*} params 
 */
export function putSeckill(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantFlashSale/' + id,
    method: 'put',
    data: params
  })
}
/**
 * 删除秒杀
 * @param {*} params 
 */
export function delSeckill(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantFlashSale/' + id,
    method: 'delete'
  })
}
/**
 * 获取秒杀总开关
 * @param {*} params 
 */
export function getSeckillSwitch(params) {
  return request({
    url: '/merchantSpike',
    method: 'get',
    params
  })
}
/**
 * 修改秒杀总开关
 * @param {*} params 
 */
export function putSeckillSwitch(params) {
  const id = params.id;
  delete params.id
  return request({
    url: '/merchantSpike/' + id,
    method: 'put',
    data: params
  })
}


//签到

/**
 * 获取签到列表
 * @param {*} params 
 */
export function getSigninList(params) {
  return request({
    url: '/merchantSignIn',
    method: 'get',
    params
  })
}
/**
 * 新增签到
 * @param {*} params 
 */
export function postSignin(params) {
  return request({
    url: '/merchantSignIn',
    method: 'post',
    data: params
  })
}
/**
 * 修改签到
 * @param {*} params 
 */
export function putSignin(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantSignIn/' + id,
    method: 'put',
    data: params
  })
}
/**
 * 删除签到
 * @param {*} params 
 */
export function delSignin(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantSignIn/' + id,
    method: 'delete',
    data: params
  })
}
/**
 * 查询签到总开关
 * @param {*} params 
 */
export function getSigninSwitch(params) {
  return request({
    url: '/merchantUnits',
    method: 'get',
    params
  })
}
/**
 * 修改签到总开关
 * @param {*} params 
 */
export function putSigninSwitch(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantUnits/' + id,
    method: 'put',
    data: params
  })
}


//模板信息

/**
 * 查询模板信息
 * @param {*} params 
 */
export function getTemplate(params) {
  return request({
    url: '/SubscribeTemplate',
    method: 'get',
    params
  })
}
/**
 * 同步模板
 * @param {*} params 
 */
export function syncTemplate(params) {
  return request({
    url: '/SubscribeTemplate',
    method: 'post',
    data: params
  })
}


//拼团

/**
 * 获取拼团商品列表
 * @param {*} params
 */
export function getTuanGoodsList(params) {
  return request({
    url: '/merchantAssemble',
    method: 'get',
    params
  })
}
/**
 * 获取未拼团商品的列表
 * @param {*} params
 */
export function getNotTuanGoodsList(params) {
  return request({
    url: '/merchantAssembleGoods',
    method: 'get',
    params
  })
}
/**
 * 拼团设置保存
 * @param {*} params
 */
export function putTuanGoods(params) {
  const assemble_id = params.assemble_id;
  delete params.assemble_id;
  return request({
    url: '/merchantAssemble/' + assemble_id,
    method: 'put',
    data: params
  })
}
/**
 * 获取订单管理列表
 * @param {*} params 
 */
export function getTuanOrderList(params) {
  return request({
    url: '/merchantAssembleOrder',
    method: 'get',
    params
  })
}
/**
 * 获取拼团管理列表
 * @param {*} params 
 */
export function getTuanManageList(params) {
  return request({
    url: '/merchantAssembleAssemble',
    method: 'get',
    params
  })
}

//充值

/**
 * 获取充值列表
 * @param {*} params 
 */
export function getPayList(params) {
  return request({
    url: '/balanceRatios',
    method: 'get',
    params
  })
}
/**
 * 新增充值
 * @param {*} params 
 */
export function postPay(params) {
  return request({
    url: '/balanceRatios',
    method: 'post',
    data: params
  })
}
/**
 * 修改充值
 * @param {*} params 
 */
export function putPay(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/balanceRatios/' + id,
    method: 'put',
    data: params
  })
}
/**
 * 删除充值
 * @param {*} params 
 */
export function delPay(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/balanceRatios/' + id,
    method: 'delete',
    data: params
  })
}

/**
 * 查询充值记录列表
 * @param {*} params 
 */
export function getPaylogList(params) {
  return request({
    url: '/balanceAccessLists',
    method: 'get',
    params
  })
}


//砍价

/**
 * 获取砍价信息
 * @param {*} params 
 */
export function getCutConfig(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantAppInfo/' + id,
    method: 'get',
    params
  })
}
/**
 * 修改砍价基本设置
 * @param {*} params 
 */
export function putCutConfig(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantBargain/' + id,
    method: 'put',
    data: params
  })
}
/**
 * 获取活动管理信息
 * @param {*} params 
 */
export function activeManage(params) {
  return request({
    url: '/merchantBargain',
    method: 'get',
    params
  })
}
/**
 * 获取发起砍价记录列表
 * @param {*} params 
 */
export function getCutSponsorList(params) {
  return request({
    url: '/merchantBargainInfo',
    method: 'get',
    params
  })
}
/**
 * 获取订单管理列表
 * @param {*} params 
 */
export function getCutOrderList(params) {
  return request({
    url: '/merchantBargainInfo',
    method: 'get',
    params
  })
}


//新人专享

/**
 * 修改新人专享设置
 * @param {*} params 
 */
export function putRecruitsConfig(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantAppInfos/' + id,
    method: 'put',
    data: params
  })
}
/**
 * 获取新人专享商品列表
 * @param {*} params 
 */
export function getRecruitsGoodsList(params) {
  return request({
    url: '/merchantRecruits',
    method: 'get',
    params
  })
}
/**
 * 删除新人专享商品
 * @param {*} params 
 */
export function delRecruitsGoods(params) {
  const id = params.id;
  delete params.id;
  return request({
    url: '/merchantRecruits/' + id,
    method: 'delete',
    data: params
  })
}
/**
 * 获取商品列表
 * @param {*} params 
 */
export function getReGoodsList(params) {
  return request({
    url: '/merchantRecruitsgoods',
    method: 'get',
    params
  })
}
/**
 * 添加新人满减商品
 * @param {*} params 
 */
export function postReGoods(params) {
  return request({
    url: '/merchantRecruits',
    method: 'post',
    data: params
  })
}


/**
 * 购物返现列表
 */

export function merchantCashback(params) {
  return request({
    url: '/merchantCashback',
    method: 'get',
    params
  })
}

/**
* 新增购物返现列表
*/

export function putMerchantCashback(params) {
  return request({
    url: '/merchantCashback',
    method: 'post',
    data: params
  })
}
/**
* 删除购物返现列表
*/

export function delMerchantCashback(params) {
  return request({
    url: '/merchantCashback/' + params.id,
    method: 'delete',
    data: params
  })
}

/**
 * 收款列表
 */

export function merchantStorePayment(params) {
  return request({
    url: '/merchantStorePayment',
    method: 'get',
    params
  })
}

/**
* 获取设置
*/

export function merchantStorePaymentConfig(params) {
  return request({
    url: '/merchantStorePaymentConfig',
    method: 'get',
    params
  })
}
/**
* 修改设置
*/

export function putmerchantStorePaymentConfig(params, id) {
  return request({
    url: '/merchantStorePaymentConfig/' + id,
    method: 'put',
    data: params
  })
}


/**
 * 获取红包(店铺, 类目, 新人, 商品)
 */

export function shopVouTypesAll(params) {
  return request({
    url: '/shopVouTypesAll',
    method: 'get',
    params
  })
}
/**
 * 获取红包(店铺, 类目, 新人, 商品)
 */

export function shopVouchersPack(params) {
  return request({
    url: '/shopVouchersPack',
    method: 'post',
    data: params,
    timeout: 10000
  })
}

/**
 * 获取应用列表
 */
export function merchantPlugin(params) {
  return request({
    url: '/merchantPlugin',
    params
  })
}
